#include <linux/kernel.h>
#include <linux/linkage.h>
#include <linux/syscalls.h>

long sys_my_print_msg(void){
    printk("linux application design lecture\n");
    printk("subclass: 02");
    printk("id: 20160570");
    printk("name: Seongchan Joe");
    return 0;
}

SYSCALL_DEFINE0(sys_my_print_msg){
    return sys_my_print_msg();
}

